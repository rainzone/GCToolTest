Pod::Spec.new do |s|
  s.name             = 'GCToolTest'
  s.version          = '1.0.0'
  s.summary          = 'aerger A short description of GCToolTest.'


  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitee.com/rainzone'

  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'guochao_ios@126.com' => 'guochao_ios@126.com' }
  s.source           = { :git => 'https://gitee.com/rainzone/GCToolTest.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'GCToolTest/Classes/**/*'

   s.dependency 'AFNetworking'
end

