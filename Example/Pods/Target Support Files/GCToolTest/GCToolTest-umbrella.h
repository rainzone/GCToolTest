#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "BHRHttpTool.h"
#import "GTMBase64.h"
#import "GTMDefines.h"
#import "NSData+AES.h"
#import "SecurityUtil.h"

FOUNDATION_EXPORT double GCToolTestVersionNumber;
FOUNDATION_EXPORT const unsigned char GCToolTestVersionString[];

