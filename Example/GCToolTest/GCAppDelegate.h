//
//  GCAppDelegate.h
//  GCToolTest
//
//  Created by guochao_ios@126.com on 06/18/2018.
//  Copyright (c) 2018 guochao_ios@126.com. All rights reserved.
//

@import UIKit;

@interface GCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
