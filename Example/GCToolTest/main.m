//
//  main.m
//  GCToolTest
//
//  Created by guochao_ios@126.com on 06/18/2018.
//  Copyright (c) 2018 guochao_ios@126.com. All rights reserved.
//

@import UIKit;
#import "GCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GCAppDelegate class]));
    }
}
