# GCToolTest

[![CI Status](https://img.shields.io/travis/guochao_ios@126.com/GCToolTest.svg?style=flat)](https://travis-ci.org/guochao_ios@126.com/GCToolTest)
[![Version](https://img.shields.io/cocoapods/v/GCToolTest.svg?style=flat)](https://cocoapods.org/pods/GCToolTest)
[![License](https://img.shields.io/cocoapods/l/GCToolTest.svg?style=flat)](https://cocoapods.org/pods/GCToolTest)
[![Platform](https://img.shields.io/cocoapods/p/GCToolTest.svg?style=flat)](https://cocoapods.org/pods/GCToolTest)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GCToolTest is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GCToolTest'
```

## Author

guochao_ios@126.com, guochao_ios@126.com

## License

GCToolTest is available under the MIT license. See the LICENSE file for more info.
